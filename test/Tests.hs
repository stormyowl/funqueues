import Test.HUnit

import BinomialQueue as Q

sampleQueue :: BinomialQueue Int
sampleQueue = Q.fromList [2, 3, 1, 4, 6, 5, 9, 8, 7]

dequeue :: Ord a => BinomialQueue a -> [a]
dequeue q = if Q.isEmpty q then [] else (Q.findMin q) : (dequeue (Q.deleteMin q))

dequeTest = TestCase $ assertEqual "Element weren't dequeued in increasing order." (dequeue sampleQueue) [1..9]
tests = TestList [TestLabel "Elements are dequeued in increasing order." dequeTest]

main :: IO Counts
main = runTestTT tests
