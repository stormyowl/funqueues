module BinomialTree (BinomialTree(..), link, singleton) where

data BinomialTree a = BinomialTree {
  rank :: Int
, root :: a
, children :: [BinomialTree a]
} deriving (Show, Eq)

singleton :: Ord a => a -> BinomialTree a
singleton x = BinomialTree 0 x []

link :: Ord a => BinomialTree a -> BinomialTree a -> BinomialTree a
link tx@(BinomialTree rx x xs) ty@(BinomialTree ry y ys)
  | rx /= ry = error "Cannot link trees of different ranks."
  | x > y = BinomialTree (ry + 1) y (tx : ys)
  | otherwise = BinomialTree (rx + 1) x (ty : xs)
