{-# LANGUAGE MultiParamTypeClasses #-}

module BinomialQueue (BinomialQueue(..), empty, isEmpty, findMin, insert, meld, deleteMin, fromList) where

import Data.List (minimum, minimumBy, reverse)
import Data.Ord (comparing)

import BinomialTree (BinomialTree(..), link, singleton)

class PriorityQueue p where
  empty :: Ord a => p a
  isEmpty :: Ord a => p a -> Bool
  findMin :: Ord a => p a -> a
  insert :: Ord a => a -> p a -> p a
  meld :: Ord a => p a -> p a -> p a
  deleteMin :: Ord a => p a -> p a
  fromList :: Ord a => [a] -> p a
  fromList = foldl (\q x -> insert x q) empty

data BinomialQueue a = BinomialQueue [BinomialTree a] deriving Show

insertTree :: Ord a => BinomialTree a -> [BinomialTree a] -> [BinomialTree a]
insertTree t [] = [t]
insertTree t trees@(y : ys)
  | rank t < rank y = t : trees
  | otherwise = insertTree (link t y) ys

meldLists :: Ord a => [BinomialTree a] -> [BinomialTree a] -> [BinomialTree a]
meldLists [] qs = qs
meldLists ps [] = ps
meldLists ps@(x : xs) qs@(y : ys)
  | rank x < rank y = x : (meldLists xs qs)
  | rank y < rank x = y : (meldLists ys ps)
  | otherwise = insertTree (link x y) (meldLists xs ys)

instance PriorityQueue BinomialQueue where
  empty = BinomialQueue []
  isEmpty (BinomialQueue ts) = null ts
  findMin (BinomialQueue []) = error "Empty queue has no elements."
  findMin (BinomialQueue ts) = minimum $ map root ts
  insert x q@(BinomialQueue qs) = BinomialQueue $ insertTree (singleton x) qs
  meld (BinomialQueue []) q = q
  meld p (BinomialQueue []) = p
  meld (BinomialQueue ps) (BinomialQueue qs) = BinomialQueue $ meldLists ps qs
  deleteMin (BinomialQueue []) = error "Empty queue has no elements."
  deleteMin (BinomialQueue ts) = BinomialQueue $ meldLists minimumTreeChildren otherTrees where
    minimumTree = minimumBy (comparing root) ts
    otherTrees = filter (/= minimumTree) ts
    minimumTreeChildren = reverse $ children minimumTree
